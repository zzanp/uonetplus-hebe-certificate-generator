# uonetplus-hebe-cert-generator

generate X.509 certificates for UONET+ gradebook hebe API

implementations:

| platform / language | RSA | ECDSA |
| ------------------- | --- | ----- |
| [node.js](node/)    | yes | no    |
| [php](php/)         | yes | no    |
| [dart](dart/)       | yes | no    |

MRs are welcome

chat on matrix: [#erupcja-uonetplus-hebe-cert-generator:laura.pm](https://matrix.to/#/#erupcja-uonetplus-hebe-cert-generator:laura.pm) / [#erupcja:laura.pm](https://matrix.to/#/#erupcja:laura.pm)

also see:

- [wulkanowy/uonet-request-signer](https://github.com/wulkanowy/uonet-request-signer#implementations-of-hebe) - ready-to-use request signing
- [wulkanowy/fake-log](https://github.com/wulkanowy/fake-log) - UONET+ imitation server for testing
- [wulkanowy/qr](https://github.com/wulkanowy/qr) - QR code data decoding and encoding
